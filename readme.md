# The Perfect Browsing Experience
## Extensions
+ [Adblock Plus](https://adblockplus.org/en/)
+ [LastPass](https://lastpass.com/)
+ [HTTPS Everywhere](https://www.eff.org/https-everywhere)
+ [Stylish](https://userstyles.org/)
+ [BandCamp Volume](https://addons.mozilla.org/En-us/firefox/addon/bandcamp-volume/)
+ [Markdown Viewer](https://addons.mozilla.org/en-us/firefox/addon/markdown-viewer/)
+ [YouTube ALL HTML5](https://addons.mozilla.org/en-US/firefox/addon/youtube-all-html5/?src=api)

## Appearance
+ [FT DeepDark 12.1.0](https://addons.mozilla.org/en-us/firefox/addon/ft-deepdark/)

## User Styles
+ [Dark Mozilla Firefox Start Page](https://userstyles.org/styles/97709/dark-mozilla-firefox-start-page)
+ [GT DeepDark for Google](https://userstyles.org/styles/102871/gt-deepdark-for-google-reposted)
+ [Dark Twitter Modified](https://userstyles.org/styles/97767/dark-twitter-modified)
+ [Darker Tumblr](https://userstyles.org/styles/84054/darker-tumblr)
+ [YouTube - Dark Grey](https://userstyles.org/styles/101409/youtube-dark-grey)
+ [YouTube - Custom Colors Video](https://userstyles.org/styles/95280/youtube-custom-colors-video-progress-bar)
+ [Reddit Slate Nights 2.0](https://userstyles.org/styles/70271/reddit-slate-nights-2-0-dark)
+ [GitHub Dark](https://userstyles.org/styles/37035/github-dark)
+ [Stack Overflow Dark](https://userstyles.org/styles/97185/stack-overflow-dark-colors)
+ [UserStyles Dark | Aperopia](https://userstyles.org/styles/105609/userstyles-dark-aperopia)
+ [4chan Dark + 4chanX V3](https://userstyles.org/styles/95865/4chan-dark-4chanx-v3)

